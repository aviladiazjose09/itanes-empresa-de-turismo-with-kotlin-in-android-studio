package com.example.itanes_empresadeturismo.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class ImagesDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase) {
        createTable(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    private fun createTable(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE $TABLE_NAME (" +
                    "Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "ByteArrayImage BLOB);"
        )
    }

    fun insertData(image: ByteArray): Boolean {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(COL_2, image)
        val res: Long = db.insert(TABLE_NAME, null, cv)

        if (res == -1L){
            return false
        }

        return true
    }

    fun deleteAllData(): Boolean {
        val db = this.writableDatabase
        try {
            val res = db.execSQL("DROP TABLE " + TABLE_NAME)
            createTable(db)
        } catch (e: SQLException) {
            return false
        }
        return true
    }

    val allData : Cursor
        get() {
            val db = this.writableDatabase
            val res = db.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY Id DESC", null)
            return res
        }

    companion object {
        val DATABASE_NAME = "images.db"
        val TABLE_NAME = "images_table"
        val COL_1 = "Id"
        val COL_2 = "ByteArrayImage"
    }
}
package com.example.itanes_empresadeturismo

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.itanes_empresadeturismo.classes.DbBitmapUtility
import com.example.itanes_empresadeturismo.database.ImagesDatabaseHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_photos.*
import java.lang.Exception

class PhotosActivity : AppCompatActivity() {
    internal var dbHelper = ImagesDatabaseHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)

        handleViewAll()

        ButtonUploadImage.setOnClickListener {
            openGallery()
        }

        ButtonDeleteAll.setOnClickListener {
            deleteAllData()
        }
    }

    private fun deleteAllData(){
        val res: Boolean = dbHelper.deleteAllData()
        if (res) {
            ActivityPhotosLayout.removeAllViews()
            showDialog("Éxito", "La eliminación fue exitosa")
        } else {
            showDialog("Error", "Ha ocurrido un error")
        }
    }

    private fun openGallery(){
        // With this, you can get any file
        //val intent = Intent("android.intent.action.GET_CONTENT")

        //This open your gallery app
        val intent = Intent(Intent.ACTION_PICK)

        intent.type = "image/*"
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 1) {
            val myBitmap: Bitmap = getBitmap(data?.data)

            val imageByte: ByteArray = DbBitmapUtility.getBytes(myBitmap)

            handleInsert(imageByte)
        }
    }

    private fun handleViewAll() {
        try {
            val res = dbHelper.allData
            if (res.count == 0) {
                showDialog("Sin imágenes", "Aún no se han agregado imágenes")
            }

            ActivityPhotosLayout.removeAllViews()

            while (res.moveToNext()) {
                val imageByte: ByteArray = res.getBlob(1)
                val imageBitmap: Bitmap = DbBitmapUtility.getImage(imageByte)

                var newView: ImageView
                newView = ImageView(this)

                ActivityPhotosLayout.addView(newView)

                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    weight = 0.5f
                    gravity = Gravity.CENTER
                }

                newView.layoutParams = params
                newView.setImageBitmap(imageBitmap)
            }
        } catch (e: Exception) {
            deleteAllData()
            showDialog("Error Fatal", "Se han eliminado todas las fotos. Recomendamos no intentar subir la misma foto")
            showDialog("Error Fatal", e.message.toString())
        }
    }

    private fun handleInsert(imageByte: ByteArray) {
        val res = dbHelper.insertData(imageByte)
        if (res) {
            handleViewAll()
            showToast("Imagen almacenada correctamente")
        } else {
            showDialog("Error", "Ha ocurrido un error")
        }
    }

    private fun getBitmap(uri: Uri?): Bitmap {
        return MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
    }

    private fun showDialog(title : String, Message : String){
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        builder.setTitle(title)
        builder.setMessage(Message)
        builder.show()
    }

    private fun showToast(text: String){
        Toast.makeText(this@PhotosActivity, text, Toast.LENGTH_LONG).show()
    }
}
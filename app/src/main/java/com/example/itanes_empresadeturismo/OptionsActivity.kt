package com.example.itanes_empresadeturismo

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_options.*
import java.util.*
import kotlin.concurrent.schedule

class OptionsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options)

        ButtonOpenMap.setOnClickListener {
            var myIntent = Intent(this, MapActivity::class.java)

            startActivity(myIntent)
        }

        ButtonOpenPhotos.setOnClickListener {
            var myIntent = Intent(this, PhotosActivity::class.java)

            startActivity(myIntent)
        }

        ButtonOpenHistory.setOnClickListener {
            var myIntent = Intent(this, HistoryActivity::class.java)

            startActivity(myIntent)
        }

    }
}